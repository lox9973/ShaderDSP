// ported from https://www.shadertoy.com/view/4dfXWj
Shader "Audio/Procedural/Mario" {
Properties {
    [ToggleUI] _Toggle0("main instrument", Float) = 1
    [ToggleUI] _Toggle1("secondary instrument", Float) = 1
    [ToggleUI] _Toggle2("bass/drum", Float) = 1
}
SubShader {
	Tags { "PreviewType"="Plane" }
	Pass {
		Tags { "LightMode"="ForwardBase" }
		Cull Off
CGPROGRAM
#pragma vertex CustomRenderTextureVertexShader
#pragma fragment frag
#include "../Shader/Procedural.hlsl"
#include "GLSL.hlsl"
float _Toggle0, _Toggle1, _Toggle2;

// Created by inigo quilez - iq/2014
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
//----------------------------------------------------------------------------------------

float noise( float x )
{
    return fract(sin(1371.1*x)*43758.5453);;
}


float saw( float x, float a)
{
    float f = fract( x );
	return clamp(f/a,0.0,1.0)-clamp((f-a)/(1.0-a),0.0,1.0);
}


    
//----------------------------------------------------------------------------------------

// main instrument
float instr1( float f, float t )
{
    float y  = 0.6*saw(1.0*f*t,0.1)*clamp( 1.2 - 1.0*0.01*f*t, 0.0, 1.0 );
	      y += 0.3*saw(2.0*f*t,0.2)*clamp( 1.2 - 2.0*0.01*f*t, 0.0, 1.0 );
	      y += 0.1*saw(4.0*f*t,0.4)*clamp( 1.2 - 4.0*0.01*f*t, 0.0, 1.0 );
          y *= 0.9 + 0.1*cos(60.0*t);
	return y;	
}

float instr2( float f, float t )
{
	return instr1(f,t);
}

// bass
float instr3( float f, float t )
{
	return 1.5*cos(0.5*6.2831*f*t)*exp(-0.005*f*t );
}

// drum
float instr4( float f, float t )
{
	float y  = noise( t );
	      y *= exp(-20.0*t );
	return 0.7*y;
		
}

// music data
float doChannel1( float time );
float doChannel2( float time );
float doChannel3( float time );
float doChannel4( float time );

//----------------------------------------------------------------------------------------
// sound shader entrypoint
//
// input: time in seconds
// ouput: stereo wave valuie at "time"
//----------------------------------------------------------------------------------------

vec2 mainSound( in int samp, float time )
{	
    vec2 y = (0.0);
if(_Toggle0)
    y += vec2(0.75,0.25)*doChannel1( time ); // main instrument
if(_Toggle1)
    y += vec2(0.25,0.75)*doChannel2( time ); // secondary instrument
if(_Toggle2)
    y += vec2(0.25,0.25)*doChannel3( time ); // bass
if(_Toggle2)
    y += vec2(0.25,0.25)*doChannel4( time ); // drum
	
    y *= 1.0 - smoothstep( 55.0, 60.0, time ); // fade out
	
	return vec2( y );
}

//----------------------------------------------------------------------------------------

float note2freq( in float x ) {	return 440.0*pow( 2.0, (x-69.0)/12.0 ); }

#define D(u,v) b+=float(u);if(t>b){x=b;n=float(v);}
#define B(u)   b+=float(u);if(t>b){x=b;}
#define tint 0.048

float doChannel1( float t )
{
  float b = 0.0;
  float n = 0.0;
  float x = 0.0;
  t /= tint;
  D( 0,76)D( 3,76)D( 6,76)D( 6,72)D( 3,76)D( 6,79)D(24,72)D( 9,67)D( 9,64)D( 9,69)
  D( 6,71)D( 6,70)D( 3,69)D( 6,67)D( 4,76)D( 4,79)D( 4,81)D( 6,77)D( 3,79)D( 6,76)
  D( 6,72)D( 3,74)D( 3,71)D( 9,72)D( 9,67)D( 9,64)D( 9,69)D( 6,71)D( 6,70)D( 3,69)
  D( 6,67)D( 4,76)D( 4,79)D( 4,81)D( 6,77)D( 3,79)D( 6,76)D( 6,72)D( 3,74)D( 3,71)
  D(15,79)D( 3,78)D( 3,77)D( 3,75)D( 6,76)D( 6,68)D( 3,69)D( 3,72)D( 6,69)D( 3,72)
  D( 3,74)D( 9,79)D( 3,78)D( 3,77)D( 3,75)D( 6,76)D( 6,84)D( 6,84)D( 3,84)D(18,79)
  D( 3,78)D( 3,77)D( 3,75)D( 6,76)D( 6,68)D( 3,69)D( 3,72)D( 6,69)D( 3,72)D( 3,74)
  D( 9,75)D( 9,74)D( 9,72)D(30,79)D( 3,78)D( 3,77)D( 3,75)D( 6,76)D( 6,68)D( 3,69)
  D( 3,72)D( 6,69)D( 3,72)D( 3,74)D( 9,79)D( 3,78)D( 3,77)D( 3,75)D( 6,76)D( 6,84)
  D( 6,84)D( 3,84)D(18,79)D( 3,78)D( 3,77)D( 3,75)D( 6,76)D( 6,68)D( 3,69)D( 3,72)
  D( 6,69)D( 3,72)D( 3,74)D( 9,75)D( 9,74)D( 9,72)D(24,72)D( 3,72)D( 6,72)D( 6,72)
  D( 3,74)D( 6,76)D( 3,72)D( 6,69)D( 3,67)D(12,72)D( 3,72)D( 6,72)D( 6,72)D( 3,74)
  D( 3,76)D(27,72)D( 3,72)D( 6,72)D( 6,72)D( 3,74)D( 6,76)D( 3,72)D( 6,69)D( 3,67)
  D(12,76)D( 3,76)D( 6,76)D( 6,72)D( 3,76)D( 6,79)D(24,72)D( 9,67)D( 9,64)D( 9,69)
  D( 6,71)D( 6,70)D( 3,69)D( 6,67)D( 4,76)D( 4,79)D( 4,81)D( 6,77)D( 3,79)D( 6,76)
  D( 6,72)D( 3,74)D( 3,71)D( 9,72)D( 9,67)D( 9,64)D( 9,69)D( 6,71)D( 6,70)D( 3,69)
  D( 6,67)D( 4,76)D( 4,79)D( 4,81)D( 6,77)D( 3,79)D( 6,76)D( 6,72)D( 3,74)D( 3,71)
  D( 9,76)D( 3,72)D( 6,67)D( 9,68)D( 6,69)D( 3,77)D( 6,77)D( 3,69)D(12,71)D( 4,81)
  D( 4,81)D( 4,81)D( 4,79)D( 4,77)D( 4,76)D( 3,72)D( 6,69)D( 3,67)D(12,76)D( 3,72)
  D( 6,67)D( 9,68)D( 6,69)D( 3,77)D( 6,77)D( 3,69)D(12,71)D( 3,77)D( 6,77)D( 3,77)
  D( 4,76)D( 4,74)D( 4,72)D(24,76)D( 3,72)D( 6,67)D( 9,68)D( 6,69)D( 3,77)D( 6,77)
  D( 3,69)D(12,71)
  return instr1( note2freq( n ), tint*(t-x) );
}

float doChannel2( float t )
{
  float b = 0.0;
  float n = 0.0;
  float x = 0.0;
  t /= tint;
  D( 0,66)D( 3,66)D( 6,66)D( 6,66)D( 3,66)D( 6,71)D(12,67)D(12,64)D( 9,60)D( 9,55)
  D( 9,60)D( 6,62)D( 6,61)D( 3,60)D( 6,60)D( 4,67)D( 4,71)D( 4,72)D( 6,69)D( 3,71)
  D( 6,69)D( 6,64)D( 3,65)D( 3,62)D( 9,64)D( 9,60)D( 9,55)D( 9,60)D( 6,62)D( 6,61)
  D( 3,60)D( 6,60)D( 4,67)D( 4,71)D( 4,72)D( 6,69)D( 3,71)D( 6,69)D( 6,64)D( 3,65)
  D( 3,62)D(15,76)D( 3,75)D( 3,74)D( 3,71)D( 6,72)D( 6,64)D( 3,65)D( 3,67)D( 6,60)
  D( 3,64)D( 3,65)D( 9,76)D( 3,75)D( 3,74)D( 3,71)D( 6,72)D( 6,77)D( 6,77)D( 3,77)
  D(18,76)D( 3,75)D( 3,74)D( 3,71)D( 6,72)D( 6,64)D( 3,65)D( 3,67)D( 6,60)D( 3,64)
  D( 3,65)D( 9,68)D( 9,65)D( 9,64)D(30,76)D( 3,75)D( 3,74)D( 3,71)D( 6,72)D( 6,64)
  D( 3,65)D( 3,67)D( 6,60)D( 3,64)D( 3,65)D( 9,76)D( 3,75)D( 3,74)D( 3,71)D( 6,72)
  D( 6,77)D( 6,77)D( 3,77)D(18,76)D( 3,75)D( 3,74)D( 3,71)D( 6,72)D( 6,64)D( 3,65)
  D( 3,67)D( 6,60)D( 3,64)D( 3,65)D( 9,68)D( 9,65)D( 9,64)D(24,68)D( 3,68)D( 6,68)
  D( 6,68)D( 3,70)D( 6,67)D( 3,64)D( 6,64)D( 3,60)D(12,68)D( 3,68)D( 6,68)D( 6,68)
  D( 3,70)D( 3,67)D(27,68)D( 3,68)D( 6,68)D( 6,68)D( 3,70)D( 6,67)D( 3,64)D( 6,64)
  D( 3,60)D(12,66)D( 3,66)D( 6,66)D( 6,66)D( 3,66)D( 6,71)D(12,67)D(12,64)D( 9,60)
  D( 9,55)D( 9,60)D( 6,62)D( 6,61)D( 3,60)D( 6,60)D( 4,67)D( 4,71)D( 4,72)D( 6,69)
  D( 3,71)D( 6,69)D( 6,64)D( 3,65)D( 3,62)D( 9,64)D( 9,60)D( 9,55)D( 9,60)D( 6,62)
  D( 6,61)D( 3,60)D( 6,60)D( 4,67)D( 4,71)D( 4,72)D( 6,69)D( 3,71)D( 6,69)D( 6,64)
  D( 3,65)D( 3,62)D( 9,72)D( 3,69)D( 6,64)D( 9,64)D( 6,65)D( 3,72)D( 6,72)D( 3,65)
  D(12,67)D( 4,77)D( 4,77)D( 4,77)D( 4,76)D( 4,74)D( 4,72)D( 3,69)D( 6,65)D( 3,64)
  D(12,72)D( 3,69)D( 6,64)D( 9,64)D( 6,65)D( 3,72)D( 6,72)D( 3,65)D(12,67)D( 3,74)
  D( 6,74)D( 3,74)D( 4,72)D( 4,71)D( 4,67)D( 3,64)D( 6,64)D( 3,60)D(12,72)D( 3,69)
  D( 6,64)D( 9,64)D( 6,65)D( 3,72)D( 6,72)D( 3,65)D(12,67) 
  return instr2( note2freq( n ), tint*(t-x) );
}

float doChannel3( float t )
{
  float b = 0.0;
  float n = 0.0;
  float x = 0.0;
  t /= tint;
  D( 0,50)D( 3,50)D( 6,50)D( 6,50)D( 3,50)D( 6,67)D(12,55)D(12,55)D( 9,52)D( 9,48)
  D( 9,53)D( 6,55)D( 6,54)D( 3,53)D( 6,52)D( 4,60)D( 4,64)D( 4,65)D( 6,62)D( 3,64)
  D( 6,60)D( 6,57)D( 3,59)D( 3,55)D( 9,55)D( 9,52)D( 9,48)D( 9,53)D( 6,55)D( 6,54)
  D( 3,53)D( 6,52)D( 4,60)D( 4,64)D( 4,65)D( 6,62)D( 3,64)D( 6,60)D( 6,57)D( 3,59)
  D( 3,55)D( 9,48)D( 9,55)D( 9,60)D( 6,53)D( 9,60)D( 3,60)D( 6,53)D( 6,48)D( 9,52)
  D( 9,55)D( 3,60)D( 6,79)D( 6,79)D( 3,79)D( 6,55)D( 6,48)D( 9,55)D( 9,60)D( 6,53)
  D( 9,60)D( 3,60)D( 6,53)D( 6,48)D( 6,56)D( 9,58)D( 9,60)D( 9,55)D( 3,55)D( 6,48)
  D( 6,48)D( 9,55)D( 9,60)D( 6,53)D( 9,60)D( 3,60)D( 6,53)D( 6,48)D( 9,52)D( 9,55)
  D( 3,60)D( 6,79)D( 6,79)D( 3,79)D( 6,55)D( 6,48)D( 9,55)D( 9,60)D( 6,53)D( 9,60)
  D( 3,60)D( 6,53)D( 6,48)D( 6,56)D( 9,58)D( 9,60)D( 9,55)D( 3,55)D( 6,48)D( 6,44)
  D( 9,51)D( 9,56)D( 6,55)D( 9,48)D( 9,43)D( 6,44)D( 9,51)D( 9,56)D( 6,55)D( 9,48)
  D( 9,43)D( 6,44)D( 9,51)D( 9,56)D( 6,55)D( 9,48)D( 9,43)D( 6,50)D( 3,50)D( 6,50)
  D( 6,50)D( 3,50)D( 6,67)D(12,55)D(12,55)D( 9,52)D( 9,48)D( 9,53)D( 6,55)D( 6,54)
  D( 3,53)D( 6,52)D( 4,60)D( 4,64)D( 4,65)D( 6,62)D( 3,64)D( 6,60)D( 6,57)D( 3,59)
  D( 3,55)D( 9,55)D( 9,52)D( 9,48)D( 9,53)D( 6,55)D( 6,54)D( 3,53)D( 6,52)D( 4,60)
  D( 4,64)D( 4,65)D( 6,62)D( 3,64)D( 6,60)D( 6,57)D( 3,59)D( 3,55)D( 9,48)D( 9,54)
  D( 3,55)D( 6,60)D( 6,53)D( 6,53)D( 6,60)D( 3,60)D( 3,53)D( 6,50)D( 9,53)D( 3,55)
  D( 6,59)D( 6,55)D( 6,55)D( 6,60)D( 3,60)D( 3,55)D( 6,48)D( 9,54)D( 3,55)D( 6,60)
  D( 6,53)D( 6,53)D( 6,60)D( 3,60)D( 3,53)D( 6,55)D( 3,55)D( 6,55)D( 3,55)D( 4,57)
  D( 4,59)D( 4,60)D( 6,55)D( 6,48)D(12,48)D( 9,54)D( 3,55)D( 6,60)D( 6,53)D( 6,53)
  D( 6,60)D( 3,60)D( 3,53)D( 6,50)
  return instr3( note2freq( n ), tint*(t-x) );
}

float doChannel4( float t )
{
  float b = 0.0;
  float x = 0.0;
  t /= tint;
  B(0)B(6)B(3)B(6)B(3)B(6)B(9)B(6)B(3)B(3)B(3)B(6)B(4)B(2)B(6)B(4)
  B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)
  B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)
  B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)
  B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)
  B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)
  B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)
  B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)
  B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)
  B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(3)B(6)B(3)B(6)
  B(9)B(6)B(3)B(3)B(3)B(6)B(3)B(6)B(3)B(6)B(9)B(6)B(3)B(3)B(3)B(6)
  B(3)B(6)B(3)B(6)B(9)B(6)B(3)B(3)B(3)B(6)B(3)B(6)B(3)B(6)B(9)B(6)
  B(3)B(3)B(3)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)
  B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)
  B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)B(6)B(4)B(2)
  B(6)B(4)B(2)B(9)B(3)B(6)B(6)B(9)B(3)B(6)B(6)B(9)B(3)B(6)B(6)B(9)
  B(3)B(6)B(6)B(9)B(3)B(6)B(6)B(9)B(3)B(6)B(6)B(9)B(3)B(6)B(6)B(9)
  B(3)B(6)B(6)B(9)B(3)B(6)B(6)B(9)B(3)B(6)B(6)
  return instr4( note2freq( 42.0 ), tint*(t-x) );
}
ENDCG
	}
}
}