#define vec2 float2
#define vec3 float3
#define vec4 float4
#define mod(x,y) ((x)%(y))
#define fract(x) frac(x)
#define mix(x,y,z) lerp(x,y,z)