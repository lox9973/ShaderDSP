Shader "Audio/BufferInput" {
Properties {
	[ToggleUI] _Interleave("Interleave", Int) = 1
	_Attenuation("Attenuation", Float) = 1
}
SubShader {
	Tags { "PreviewType"="Plane" } // prevent freezing Unity editor
	Pass {
CGPROGRAM
#pragma target 3.5
#pragma vertex CustomRenderTextureVertexShader
#pragma fragment frag

static const uint _InputSize = 4096;
cbuffer InputXBuffer {
	float _InputX[_InputSize] : packoffset(c0);  
	float _InputX0[1023] : packoffset(c0);
	float _InputX1[1023] : packoffset(c1023);
	float _InputX2[1023] : packoffset(c2046);
	float _InputX3[1023] : packoffset(c3069);
	float _InputX4[   4] : packoffset(c4092);
};
cbuffer InputYBuffer {
	float _InputY[_InputSize] : packoffset(c0);  
	float _InputY0[1023] : packoffset(c0);
	float _InputY1[1023] : packoffset(c1023);
	float _InputY2[1023] : packoffset(c2046);
	float _InputY3[1023] : packoffset(c3069);
	float _InputY4[   4] : packoffset(c4092);
};
uint _Interleave;
float _Attenuation;

#include "UnityCustomRenderTexture.cginc"
float4 frag(v2f_customrendertexture I) : SV_Target {
	float2 texcoord = I.localTexcoord;
	float2 texsize = _CustomRenderTextureInfo.xy;
	// uint2 coord = floor(texcoord * texsize);
	uint index = dot(floor(texcoord * texsize), float2(1, texsize.x));

	if(texcoord.x < 0)
		return _InputX0[0] + _InputX1[0] + _InputX2[0] + _InputX3[0] + _InputX4[0]
			 + _InputY0[0] + _InputY1[0] + _InputY2[0] + _InputY3[0] + _InputY4[0];

	
	float4 v = 0;
	if(!_Interleave)
		v.xy = float2(_InputX[index], _InputY[index]);
	else if((index *= 2) < _InputSize)
		v.xy = float2(_InputX[index+0], _InputX[index+1]);
	else index-=_InputSize,
		v.xy = float2(_InputY[index+0], _InputY[index+1]);
	return _Attenuation*v;
}
ENDCG
	}
}
}