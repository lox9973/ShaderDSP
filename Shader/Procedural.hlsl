const static uint _SampleRate = 48000;
const static float iSampleRate = _SampleRate;

uint _SampleStart, _SampleStop;

#include "UnityCustomRenderTexture.cginc"
static float2 _BufferSize = _CustomRenderTextureInfo.xy; // use render target size
float2 mainSound(int samp, float time);
float2 frag(v2f_customrendertexture I) : SV_Target {
	float2 texcoord = I.localTexcoord;
	uint bufferLen = _BufferSize.x * _BufferSize.y;
	uint index = dot(floor(texcoord * _BufferSize), float2(1, _BufferSize.x));
	// index += (_SampleStart/bufferLen + (_SampleStart%bufferLen > index)) * bufferLen;

	index += (_SampleStop/bufferLen - uint(_SampleStop%bufferLen <= index)) * bufferLen;
	if(_SampleStop/bufferLen < uint(_SampleStop%bufferLen <= index))
		return 0;

	float t = float(index / _SampleRate) + float(index % _SampleRate) / _SampleRate;
	return mainSound(index, t);
}