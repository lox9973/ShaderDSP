Shader "Audio/HRTF" {
Properties {
	_MainTex("_MainTex", 3D) = "black" {}
	_Volume("Volume", Float) = 1
	// _MainTex_Size("_MainTex_size", Vector)
}
SubShader {
	Tags { "PreviewType"="Plane" } // prevent freezing Unity editor
	Pass {
CGPROGRAM
#pragma target 3.5
#pragma vertex CustomRenderTextureVertexShader
#pragma fragment frag

Texture3D _MainTex;
SamplerState sampler_MainTex;
float _Volume;

#include "UnityCustomRenderTexture.cginc"

float4 frag(v2f_customrendertexture I) : SV_Target {
	float2 texcoord = I.localTexcoord;
	float2 texsize = _CustomRenderTextureInfo.xy;
	uint2 coord = floor(texcoord * texsize);

	float3 size;
	_MainTex.GetDimensions(size.x, size.y, size.z);

	float elevation = 0;
	float azimuth = frac(_Time.y/10)*360;

	float t = frac(_Time.y/10)*2;
	t += smoothstep(0,1,frac(t))-frac(t);
	azimuth = t*180+90;

	float3 sampleCoord = {(azimuth-0)/3, (elevation+90)/6, coord.x};
	return _MainTex.Sample(sampler_MainTex, (sampleCoord+0.5)/size) * _Volume;
}
ENDCG
	}
}
}