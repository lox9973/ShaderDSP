Shader "Audio/ConvoOutput" {
Properties {
	[NoScaleOffset] _Input("Input", 2D) = "black" {}
}
SubShader {
	Tags { "PreviewType"="Plane" } // prevent freezing Unity editor
	Pass {
		Blend One OneMinusSrcAlpha, Zero Zero
CGPROGRAM
#pragma target 3.5
#pragma vertex CustomRenderTextureVertexShader
#pragma fragment frag

Texture2D _Input;
uint _SampleStart, _SampleStop;

#include "FFT.hlsl"
#include <UnityCustomRenderTexture.cginc>
static float2 _BufferSize = _CustomRenderTextureInfo.xy; // use render target size
float4 frag(v2f_customrendertexture I) : SV_Target {
	float2 texcoord = I.localTexcoord;
	uint bufferLen = _BufferSize.x * _BufferSize.y;
	uint index = dot(floor(texcoord * _BufferSize), float2(1, _BufferSize.x));
	index += (_SampleStart/bufferLen + (_SampleStart%bufferLen > index)) * bufferLen;

	uint2 size; _Input.GetDimensions(size.x, size.y);

	uint offset = index-_SampleStart;
	if(offset >= size.x * size.y + (_SampleStop-_SampleStart))
		return 0;
	if(offset >= size.x * size.y)
		return float4(0,0,0,1);

	uint2 coord = uint2(offset%size.x, offset/size.x); // nowrap
	float4 v = unpack_real(_Input[int2(coord.x/2, coord.y)] + _Input[int2(coord.x/2+uint(size.x/2), int(coord.y)-1)]);
	return float4(coord.x % 2 == 0 ? v.xy : v.zw, 0,0);
}
ENDCG
	}
}
}