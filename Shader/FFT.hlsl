float4 pack_real(float2 real0, float2 real1) {
	return float4(real0.xy, real1.xy).xzyw;
}
float4 unpack_real(float4 complex2) {
	return complex2.xzyw;
}
// compute c*a+b in the format float4(cplx0, cplx1)
float4 complex2_mad(float4 c, float4 a, float4 b) {
	return float4(c.xz+a.xz*b.xz-a.yw*b.yw, c.yw+a.xz*b.yw+a.yw*b.xz).xzyw;
}
// pointwise multiply packed fourier transform
float4 complex2_mul_packed(float4 x, float4 y, float4 x_reverse, float4 y_reverse, float angle) {
	return complex2_mad(complex2_mad(0, x, y),
		complex2_mad(0, x+x_reverse*float2(-1,1).xyxy, y+y_reverse*float2(-1,1).xyxy),
		(float2(1+cos(angle), sin(angle))/-4).xyxy);
}
