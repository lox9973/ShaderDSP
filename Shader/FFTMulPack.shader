Shader "Audio/FFT-MultiplyPacked" {
Properties {
	[NoScaleOffset] _Input("Input", 2D) = "black" {}
}
SubShader {
	Tags { "PreviewType"="Plane" } // prevent freezing Unity editor
	Pass {
CGPROGRAM
#pragma target 3.5
#pragma vertex CustomRenderTextureVertexShader
#pragma fragment frag

Texture2D _Input;

#include "FFT.hlsl"
#include <UnityCustomRenderTexture.cginc>
float4 frag(v2f_customrendertexture I) : SV_Target {
	float2 texcoord = I.localTexcoord;
	float2 texsize = _CustomRenderTextureInfo.xy;
	uint2 coord = floor(texcoord * texsize);

	float f = -2*UNITY_PI / texsize.x;
	uint coord_x_reverse = coord.x == 0 ? 0 : uint(texsize.x)-coord.x;
	return complex2_mul_packed(
		_Input[uint2(coord.x, 0)], _Input[uint2(coord.x, coord.y+1)],
		_Input[uint2(coord_x_reverse, 0)], _Input[uint2(coord_x_reverse, coord.y+1)],
		f*coord.x);
}
ENDCG
	}
}
}