Shader "Audio/ConvoInput" {
Properties {
	[NoScaleOffset] _Input("Input", 2D) = "black" {}
	[NoScaleOffset] _Kernel("Kernel", 2D) = "black" {}
	_Mix("Mix", Range(0, 1)) = 1
	[ToggleUI] _Mono("Mono", Int) = 0
}
SubShader {
	Tags { "PreviewType"="Plane" } // prevent freezing Unity editor
	Pass {
CGPROGRAM
#pragma target 3.5
#pragma vertex CustomRenderTextureVertexShader
#pragma fragment frag

Texture2D _Input;
Texture2D _Kernel;
uint _SampleStart, _SampleStop;
float _Mix;
uint _Mono;

#include "FFT.hlsl"
#include <UnityCustomRenderTexture.cginc>
float4 frag(v2f_customrendertexture I) : SV_Target {
	float2 texcoord = I.localTexcoord;
	float2 texsize = _CustomRenderTextureInfo.xy;
	uint2 coord = floor(texcoord * texsize);
	if(coord.y == 0) {
		uint index = _SampleStart + coord.x*2;
		if(index >= _SampleStop)
			return 0;

		uint2 size; _Input.GetDimensions(size.x, size.y);
		uint2 coord2 = {index%size.x, index/size.x%size.y}; // wrap
		float4 v = pack_real(_Input[uint2(coord2.x+0, coord2.y)], _Input[uint2(coord2.x+1, coord2.y)]);
		if(_Mono)
			v = ((v.xy+v.zw)/2).xyxy;
		return v;
	} else {
		uint index = (coord.x + uint(texsize.x)*(coord.y-1))*2;

		uint2 size; _Kernel.GetDimensions(size.x, size.y);
		uint2 coord2 = {index%size.x, index/size.x}; // nowrap
		float4 v = pack_real(_Kernel[uint2(coord2.x+0, coord2.y)], _Kernel[uint2(coord2.x+1, coord2.y)]);
		return lerp(index==0 ? pack_real(1,0) : 0, v, _Mix);
	}
}
ENDCG
	}
}
}