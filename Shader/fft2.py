import numpy as np

def pack(x):
	return x[0::2] + 1j * x[1::2]
def unpack(x):
	return np.transpose((np.real(x), np.imag(x))).reshape(-1)
def star(x):
	return np.conjugate(np.append(x[1:], x[0]))[::-1]
def multiply_packed(x, y):
	n = len(x)
	w = np.exp(-2j * np.pi / n * np.arange(n))
	return (x*y) - (1+w)/4 * (x-star(x))*(y-star(y))

def test_convolution():
	n = 1024
	x = np.random.rand(2*n)
	y = np.random.rand(2*n)
	z = unpack(np.fft.ifft(multiply_packed(np.fft.fft(pack(x)), np.fft.fft(pack(y)))))
	assert np.allclose(z, np.fft.ifft(np.fft.fft(x) * np.fft.fft(y)))

#####

def test_fft():
	radices = [16, 8, 4]
	n = np.prod(radices)
	x = np.random.rand(n) + np.random.rand(n) * 1j
	y = np.array(x)
	p = 1
	for radix in radices:
		fft_step(y, radix, p)
		p *= radix
	assert np.allclose(y, np.fft.fft(x))

def fft_step(x, radix, stride):
	n = len(x)
	n0, n1, n2 = stride, radix, n//stride//radix
	y = np.zeros_like(x)
	for i in range(n):
		i0, i1, i2 = i%n0, i//n0%n1, i//n0//n1
		y[i] = sum(x[(i0+n0*i2)+(n0*n2)*k] * np.exp((i2*i1+n2*i1*k)/(n1*n2) * -2j*np.pi) for k in range(n1))
		# ijk <- hik: (hi)*j

	x[:] = y

if __name__ == '__main__':
	# test_convolution()
	test_fft()
	print("done")