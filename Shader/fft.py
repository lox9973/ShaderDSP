import numpy as np

def pack(x):
	return x[0::2] + 1j * x[1::2]
def unpack(x):
	return np.transpose((np.real(x), np.imag(x))).reshape(-1)
def star(x):
	return np.conjugate(np.append(x[1:], x[0]))[::-1]
def multiply_packed(x, y):
	n = len(x)
	w = np.exp(-2j * np.pi / n * np.arange(n))
	return (x*y) - (1+w)/4 * (x-star(x))*(y-star(y))

def test_convolution():
	n = 1024
	x = np.random.rand(2*n)
	y = np.random.rand(2*n)
	z = unpack(np.fft.ifft(multiply_packed(np.fft.fft(pack(x)), np.fft.fft(pack(y)))))
	assert np.allclose(z, np.fft.ifft(np.fft.fft(x) * np.fft.fft(y)))

def fft_init(x, radices):
	return np.transpose(x.reshape(radices)).reshape((1, -1))
def fft_step(x, radix):
	m = x.shape[0]
	w = np.exp(-2j * np.pi / (m*radix) * np.outer(np.arange(m*radix), np.arange(radix))).reshape((radix, m, 1, radix))
	return np.sum(x.reshape((1, m, -1, radix)) * w, axis=3).reshape((radix*m, -1))

def test_fft():
	radices = [16, 8, 4]
	n = np.prod(radices)
	x = np.random.rand(n) + np.random.rand(n) * 1j
	y = fft_init(x, radices)
	for r in radices:
		y = fft_step(y, r)
	assert np.allclose(y.reshape((-1,)), np.fft.fft(x))

if __name__ == '__main__':
	test_convolution()
	test_fft()
	print("done")