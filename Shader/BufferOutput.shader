Shader "Audio/BufferOutput" {
Properties {
	_Input("Input", 2D) = "black" {}
	_Latency("Latency", Int) = 4096
	_Attenuation("Attenuation", Float) = 1
}
SubShader {
	Tags { "PreviewType"="Plane" } // prevent freezing Unity editor
	Pass {
CGPROGRAM
#pragma target 3.5
#pragma vertex CustomRenderTextureVertexShader
#pragma fragment frag

Texture2D _Input;
uint _SampleStart;
uint _Latency;
float _Attenuation;

#include "UnityCustomRenderTexture.cginc"
static float2 _BufferSize = _CustomRenderTextureInfo.xy; // use render target size
float4 frag(v2f_customrendertexture I) : SV_Target {
	_SampleStart /= 2;

	float2 texcoord = I.localTexcoord;
	uint bufferLen = _BufferSize.x * _BufferSize.y;
	uint index = dot(floor(texcoord * _BufferSize), float2(1, _BufferSize.x));
	index += (_SampleStart/bufferLen + (_SampleStart%bufferLen > index)) * bufferLen;
	index = index*2 - _Latency;

	uint2 size; _Input.GetDimensions(size.x, size.y);
	return float4(
		_Input[uint2(index%size.x+0, index/size.x%size.y)].xy,
		_Input[uint2(index%size.x+1, index/size.x%size.y)].xy) * _Attenuation;
}
ENDCG
	}
}
}