Shader "Audio/FFT" {
Properties {
	[NoScaleOffset] _Input("Input", 2D) = "black" {}
	[ToggleUI] _Inverse("Inverse", Int) = 0
	[ToggleUI] _FirstPass("FirstPass", Int) = 0
	_Radix("Radix", Vector) = (1,1,1,1)
}
SubShader {
	Tags { "PreviewType"="Plane" } // prevent freezing Unity editor
	Pass {
CGPROGRAM
#pragma target 3.5
#pragma vertex CustomRenderTextureVertexShader
#pragma fragment frag

Texture2D _Input;
int _Inverse;
int _FirstPass;
float4 _Radix;

#include "FFT.hlsl"
#include <UnityCustomRenderTexture.cginc>
float4 frag(v2f_customrendertexture I) : SV_Target {
	float2 texcoord = I.localTexcoord;
	float2 texsize = _CustomRenderTextureInfo.xy;
	uint2 coord = floor(texcoord * texsize);
	uint4 radix = _Radix;

	uint start, step, prod = radix.y*radix.z*radix.w;
	if(_FirstPass)
		step = prod, start = (coord.x%radix.y * radix.z + coord.x/radix.y%radix.z) * radix.w + coord.x/radix.y/radix.z%radix.w;
	else
		step = 1, start = coord.x % (uint(texsize.x)/radix.x) * radix.x;

	float f = (_Inverse ? +2*UNITY_PI : -2*UNITY_PI) / texsize.x * (coord.x-coord.x%prod);
	float4 sum = 0;
	for(uint k=0; k<radix.x; k++)
		sum = complex2_mad(sum, float2(cos(f*k), sin(f*k)).xyxy, _Input[uint2(start+step*k, coord.y)]);

	if(_FirstPass && _Inverse)
		sum /= texsize.x; // IDFT normalize
	return sum;
}
ENDCG
	}
}
}