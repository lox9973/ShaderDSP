using UnityEngine;
using UdonSharp;
using VRC.Udon;
namespace ShaderAudio {
[UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
public class AudioHub: UdonSharpBehaviour {
	private void OnEnable() {
		ApplyIO();
		ApplyFilter();
		ApplyAudioShader();
	}

	[Header("Input")]
	public AudioInputNode inputNode;
	public Material[] inputMaterials;
	public UdonSharpBehaviour[] inputAudios;
	public GameObject[] inputPanels;
	public UnityEngine.UI.Dropdown inputDropdown;
	
	[Header("Output")]
	public AudioOutputNode outputNode;
	public Material[] outputMaterials;
	public UdonSharpBehaviour[] outputAudios;
	public GameObject[] outputPanels;
	public UnityEngine.UI.Dropdown outputDropdown;

	public UnityEngine.UI.Slider latencySlider;
	public UnityEngine.UI.Text latencyText;
	
	[Header("Effect")]
	public GameObject[] effectPanels;
	public Texture[] effectOutputTextures;
	public UnityEngine.UI.Dropdown effectDropdown;
	
	private int inputDropdownLast = -1;
	private int outputDropdownLast = -1;
	public void ApplyIO() {
		if(outputDropdown.value + inputDropdown.value == 1) { // forbid listener <-> source (loopback/performance)
			if(inputDropdownLast == inputDropdown.value)
				inputDropdown.value = outputDropdown.value;
			else
				outputDropdown.value = inputDropdown.value;
		} else if(outputDropdown.value == 1 && inputDropdown.value != 1) // forbid non-listener -> listener (performance)
			outputDropdown.value = 0;
		inputDropdownLast = inputDropdown.value;
		outputDropdownLast = outputDropdown.value;

		foreach(var go in inputPanels)
			go.SetActive(false);
		inputPanels[inputDropdown.value].SetActive(true);

		foreach(var go in outputPanels)
			go.SetActive(false);
		outputPanels[outputDropdown.value].SetActive(true);

		inputNode.enabled = false;
		outputNode.enabled = false;
		// Debug.Log("Update IO Node");
		inputNode.material = inputMaterials[inputDropdown.value];
		inputNode.audioInterface = (UdonBehaviour)(Component)inputAudios[inputDropdown.value];
		outputNode.material = outputMaterials[outputDropdown.value];
		outputNode.audioInterface = (UdonBehaviour)(Component)outputAudios[outputDropdown.value];
		if(inputDropdown.value == 2)
			outputNode.clock = 0;
		outputNode.enabled = true;
		inputNode.enabled = true;

		ApplyEffect();
		ApplyLatency();
	}
	public void ApplyLatency() {
		var latency = Mathf.RoundToInt(latencySlider.value) * 1024;
		outputNode.material.SetInt("_Latency", latency);
		if(latencyText)
			latencyText.text = string.Format("{0} ms", latency / 48);
	}
	public void ApplyEffect() {
		var tex = effectOutputTextures[effectDropdown.value];
		outputNode.material.SetTexture("_Input", tex ? tex : inputNode.renderTexture);

		foreach(var go in effectPanels)
			go.SetActive(false);
		effectPanels[effectDropdown.value].SetActive(true);
	}

	[Header("Convolution")]
	public Material filterInputMaterial;
	public UnityEngine.UI.Slider filterWeightSlider;
	public UnityEngine.UI.Dropdown filterDropdown;
	public AudioClipLoader filterLoader;
	public AudioClip[] filterClips;
	public AudioSource filterAudioSource;
	public void ApplyFilterWeight() {
		filterInputMaterial.SetFloat("_Mix", filterWeightSlider.value);
	}
	public void ApplyFilter() {
		filterLoader.clip = filterClips[filterDropdown.value];
		filterLoader.enabled = true;
		ApplyFilterWeight();
	}
	public void PlayFilter() {
		filterAudioSource.PlayOneShot(filterLoader.clip);
	}

	[Header("AudioShader")]
	public Material audioShaderMaterial;
	public Object[] audioShaders;
	public UnityEngine.UI.Dropdown audioShaderDropdown;
	public UnityEngine.UI.Toggle[] audioShaderToggles;
	public void ApplyAudioShader() {
		var index = audioShaderDropdown.value;
		audioShaderMaterial.shader = (Shader)audioShaders[index];
		ApplyAudioShaderToggle();
		// restart timer
		outputNode.enabled = false;
		outputNode.clock = 0;
		outputNode.enabled = true;
	}
	public void ApplyAudioShaderToggle() {
		for(int i=0; i<audioShaderToggles.Length; i++)
			audioShaderMaterial.SetFloat($"_Toggle{i}", audioShaderToggles[i].isOn ? 1f : 0f);
	}
}
}