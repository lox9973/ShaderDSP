using UnityEngine;
using UdonSharp;
namespace ShaderAudio {
[UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
public class RenderOrder: UdonSharpBehaviour {
	public UnityEngine.UI.Text debugText;
	public Material material;

	private Rect rect;
	private Texture2D tempTex;
	private Vector4 weight = new Vector4(1,1<<8,1<<16,1<<24);

	private void Start() {
		var crt = (CustomRenderTexture)GetComponent<Camera>().targetTexture;
		crt.material = material;
		rect = new Rect(0, 0, crt.width, crt.height);
		tempTex = (Texture2D)crt.initializationTexture;
	}
	private void LateUpdate() {
		material.SetInt("_FrameIndex", Time.frameCount);
	}
	private void OnPostRender() {
		tempTex.ReadPixels(rect, 0, 0, false);
		var color = tempTex.GetPixel(0,0);
		var index = Mathf.RoundToInt(Vector4.Dot((Vector4)color, weight));
		debugText.text = $"{Time.frameCount-index}";
	}
}
}