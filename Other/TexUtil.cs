#if UNITY_EDITOR
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace ShaderAudio {
public class TexUtil {
	[MenuItem("ShaderAudio/CreateClipTexture")]
	static void CreateClipTexture() {
		var clip = (AudioClip)Selection.activeObject;
		var channels = clip.channels;
		var length = clip.samples;
		var samples = new float[length * channels];
		clip.LoadAudioData();
		clip.GetData(samples, 0);

		var width = 256;
		var height = (length+255)/256;
		var colors = new Color[width*height];
		var sum = 0f;
		for(int i=0; i<length * channels; i++)
			sum += samples[i]*samples[i];
		sum = Mathf.Sqrt(sum);
		// Debug.Log($"{samples.Length/channels} x {channels}: {width} x {height}");

		var offset0 = 0;
		var offset1 = Mathf.Min(1, channels-1);
		for(int i=0; i<length; i++) {
			colors[i].r = samples[i*channels+offset0]/sum;
			colors[i].g = samples[i*channels+offset1]/sum;
		}

		var tex = new Texture2D(width, height, TextureFormat.RGHalf, false);
		tex.SetPixels(colors, 0);
		tex.Apply(true);

		var path = System.IO.Path.ChangeExtension(AssetDatabase.GetAssetPath(clip), ".texture2D");
		AssetDatabase.CreateAsset(tex, path);
	}
	[MenuItem("ShaderAudio/ResizeTexture2DToRT")]
	static void ResizeTexture2DToRT() {
		var tex = (Texture2D)Selection.objects.First(x => x is Texture2D);
		var rt = (RenderTexture)Selection.objects.First(x => x is RenderTexture);
		tex.Resize(rt.width, rt.height, TextureFormat.RGBAHalf, false);
	}
	[MenuItem("ShaderAudio/LoadHRTF")]
	static void LoadHRTF() {
		var path = AssetDatabase.GetAssetPath(Selection.activeObject);
		var width = 120;
		var height = 31;
		var depth = 512;
		var colors = new Color[width*height*depth];
		using (var reader = new System.IO.BinaryReader(System.IO.File.Open(path, System.IO.FileMode.Open))) {
			for(int i=0; i<colors.Length; i++) {
				colors[i].r = reader.ReadSingle();
				colors[i].g = reader.ReadSingle();
			}
		}
		var tex = new Texture3D(width, height, depth, TextureFormat.RGHalf, false);
		tex.filterMode = FilterMode.Bilinear;
		tex.wrapModeU = TextureWrapMode.Repeat;
		tex.wrapModeV = TextureWrapMode.Clamp;
		tex.wrapModeW = TextureWrapMode.Clamp;
		tex.SetPixels(colors);
		AssetDatabase.CreateAsset(tex, System.IO.Path.ChangeExtension(path, ".texture2D"));
	}
	[MenuItem("ShaderAudio/AddObjectToAsset")]
	static void AddObjectToAsset() {
		var target = Selection.activeObject;
		var sources = Selection.objects.Where(x => x != target)
			.SelectMany(x => AssetDatabase.IsSubAsset(x) ? new Object[]{x} :
				AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(x))).ToArray();
		var path = AssetDatabase.GetAssetPath(target);
		
		Debug.Log($"target={target}");

		var sourceIdxs = sources.Select((x,i) => (x,i)).ToDictionary(p => p.Item1, p => p.Item2);
		var overrides = new List<(string,int)>[sources.Length];
		for(int i=0; i<sources.Length; i++) {
			overrides[i] = new List<(string,int)>();
			for(var it = new SerializedObject(sources[i]).GetIterator(); it.Next(true); ) 
				if(it.propertyType == SerializedPropertyType.ObjectReference && it.objectReferenceValue)
					if(sourceIdxs.TryGetValue(it.objectReferenceValue, out var idx))
						overrides[i].Add((it.propertyPath, idx));
		}
		var clones = new Object[sources.Length];
		for(int i=0; i<sources.Length; i++) {
			clones[i] = Object.Instantiate(sources[i]);
			clones[i].name = sources[i].name;
			AssetDatabase.AddObjectToAsset(clones[i], path);
			// Debug.Log($"{sources[i]}:");
			// foreach(var (propPath, idx) in overrides[i])
			// 	Debug.Log($"  {propPath}: {sources[idx]}");
		}
		for(int i=0; i<sources.Length; i++) {
			var so = new SerializedObject(clones[i]);
			foreach(var (propPath, idx) in overrides[i])
				so.FindProperty(propPath).objectReferenceValue = clones[idx];
			so.ApplyModifiedPropertiesWithoutUndo();
		}
	}
	[MenuItem("ShaderAudio/RemoveObjectFromAsset")]
	static void RemoveObjectFromAsset() {
		var o = Selection.activeObject;
		AssetDatabase.RemoveObjectFromAsset(o);
		Object.DestroyImmediate(o);
	}
}
}
#endif