Shader "Audio/Test/RenderOrderCRT" {
Properties {
	_FrameIndex("_FrameIndex", Int) = 0
}
SubShader {
	Tags { "PreviewType"="Plane" } // prevent freezing Unity editor
	Pass {
CGPROGRAM
#pragma target 3.5
#pragma vertex CustomRenderTextureVertexShader
#pragma fragment frag

int _FrameIndex;

#include "UnityCustomRenderTexture.cginc"
float4 frag(v2f_customrendertexture I) : SV_Target {
	uint index = _FrameIndex;
	float4 color = (index >> uint4(0,8,16,24)) & 255;
	return color;
}
ENDCG
	}
}
}