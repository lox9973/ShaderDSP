using UnityEngine;
using UdonSharp;
namespace ShaderAudio {
[UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
public class AudioFilterStream: UdonSharpBehaviour {
	[System.NonSerialized] public int clock = 0;
	[System.NonSerialized] public float[] rbuffer; // stereo layout: LR..LR
	[System.NonSerialized] public Color[] wbuffer; // stereo layout: (LRLR)..(LRLR)
	[System.NonSerialized] public bool interleave = true;
	[System.NonSerialized] public float attenuation = 1f;

	private Color color;
	private Color[] temp = new Color[0];
	private void OnAudioFilterRead(float[] data, int channels) {
		var ndata = data.Length;
		var total = clock*channels+ndata;
		var rbuf = rbuffer;
		var wbuf = wbuffer;
		if(rbuf != null) {
			var nbuf = rbuf.Length;
			var n = Mathf.Min(nbuf, ndata);
			var i = (total-n) % nbuf;
			var m = Mathf.Min(n, nbuf-i);
			System.Array.Copy(data, 0, rbuf, i, m);
			System.Array.Copy(data, m, rbuf, 0, n-m);
		}
		var i0 = ndata;
		if(wbuf != null) {
			var nbuf = wbuf.Length;
			var n = Mathf.Min(nbuf, ndata/4);
			var i = (total/4-n) % nbuf;
			var m = Mathf.Min(n, nbuf-i);
			if(temp.Length < nbuf)
				temp = new Color[nbuf];
			System.Array.Copy(wbuf, i, temp, 0, m);
			System.Array.Copy(wbuf, 0, temp, m, n-m);
			i0 = ndata - n*4;
		}
		// note: advance clock AFTER data is processed
		// however, in order to reduce temporary clock drift caused by slow unpacking,
		// wbuf is processed as an array copy as above, and unpacked after clock is set
		clock = total/channels;
		for(int i = i0, j = 0; i < ndata;) { // should use Buffer.BlockCopy if it's exposed
			color = temp[j]; ++j; data[i] = color.r; data[++i] = color.g; data[++i] = color.b; data[++i] = color.a; ++i;
			color = temp[j]; ++j; data[i] = color.r; data[++i] = color.g; data[++i] = color.b; data[++i] = color.a; ++i;
			color = temp[j]; ++j; data[i] = color.r; data[++i] = color.g; data[++i] = color.b; data[++i] = color.a; ++i;
			color = temp[j]; ++j; data[i] = color.r; data[++i] = color.g; data[++i] = color.b; data[++i] = color.a; ++i;
		}
	}
	// don't implement other events here! multithreading will crash UdonBehaviour
	private float[] onAudioFilterReadData = null;
	private int onAudioFilterReadChannels = 0;
	public void _onAudioFilterRead() { // polyfill OnAudioFilterRead for UdonSharp
		OnAudioFilterRead(onAudioFilterReadData, onAudioFilterReadChannels);
	}
}
}