using UnityEngine;
using UdonSharp;
using VRC.Udon;
using VRC.SDKBase;
namespace ShaderAudio {
[UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
public class AudioPipeline: UdonSharpBehaviour {
	[Header("Input")]
	public Material inputMaterial;
	public UdonBehaviour inputAudio;
	public CustomRenderTexture inputCRT;

	[Header("Output")]
	public Material outputMaterial;
	public UdonBehaviour outputAudio;
	public CustomRenderTexture outputCRT;

	[Header("Effect")]
	public Material[] inputSyncMaterials;
	public Material[] outputSyncMaterials;
	public Texture effectCRT;

	private int wclock;
	private Color[] wbuffer;
	private Texture2D tempTex;
	private Rect rect;

	private int rclock;
	private float[] rbuffer;
	private float[] temp0, temp4;

	const int sampleSize = 4096;
	const int maxArraySize = 1023;
	const int remArraySize = sampleSize % maxArraySize;
	const int offsetX0 = maxArraySize*0;
	const int offsetX1 = maxArraySize*1;
	const int offsetX2 = maxArraySize*2;
	const int offsetX3 = maxArraySize*3;
	const int offsetX4 = maxArraySize*4;
	const int offsetY0 = sampleSize+maxArraySize*0;
	const int offsetY1 = sampleSize+maxArraySize*1;
	const int offsetY2 = sampleSize+maxArraySize*2;
	const int offsetY3 = sampleSize+maxArraySize*3;
	const int offsetY4 = sampleSize+maxArraySize*4;

	public UnityEngine.UI.Text debugText;
	private System.Diagnostics.Stopwatch stopWatch;
	private float avgTime;
	private void OnEnable() {
		stopWatch = new System.Diagnostics.Stopwatch();
		outputCRT.material = this.outputMaterial;
		inputCRT.material = this.inputMaterial;
		var inputMaterial = inputCRT.material;
		var outputMaterial = outputCRT.material;
		if(outputMaterial)
			outputMaterial.SetTexture("_Input", effectCRT ? effectCRT : inputCRT);
		if(outputAudio) {
			Debug.Log($"Enable {outputAudio} <- {outputMaterial}");
			GetComponent<Camera>().targetTexture = outputCRT;
			rect = new Rect(0, 0, outputCRT.width, outputCRT.height);
			wclock = 0;
			wbuffer = new Color[outputCRT.width * outputCRT.height];
			tempTex = (Texture2D)outputCRT.initializationTexture;
			if(tempTex.width != outputCRT.width)
				Debug.LogError("tempTex doesn't match renderTexture");
			outputAudio.SetProgramVariable("clock", wclock);
			outputAudio.SetProgramVariable("wbuffer", wbuffer);
			outputAudio.enabled = true;
		}
		
		if(inputAudio) {
			Debug.Log($"Enable {inputAudio} -> {inputMaterial}");
			rclock = wclock;
			rbuffer = new float[sampleSize*2];
			temp0 = new float[maxArraySize];
			temp4 = new float[remArraySize];
			inputAudio.SetProgramVariable("clockSource", outputAudio);
			inputAudio.SetProgramVariable("clock", rclock);
			inputAudio.SetProgramVariable("rbuffer", rbuffer);
			inputAudio.enabled = true;
			inputMaterial.SetInt("_Interleave", (bool)inputAudio.GetProgramVariable("interleave") ? 1 : 0);
		}
	}
	private void OnDisable() {
		if(Utilities.IsValid(outputAudio)) { // prevent error at unload
			Debug.Log($"Disable {outputAudio} <- {outputMaterial}");
			System.Array.Clear(wbuffer, 0, wbuffer.Length); // flush leftover sound
			outputAudio.enabled = outputAudio.GetProgramVariable("rbuffer") != null;
			outputAudio.SetProgramVariable("wbuffer", null);
		}
		if(Utilities.IsValid(inputAudio)) { // prevent error at unload
			Debug.Log($"Disable {inputAudio} -> {inputCRT.material}");
			inputAudio.enabled = inputAudio.GetProgramVariable("wbuffer") != null;
			inputAudio.SetProgramVariable("rbuffer", null);
		}
	}
	private void LateUpdate() {
		var sampleStart = 0;
		var sampleStop = 0;
		var inputMaterial = inputCRT.material;
		var outputMaterial = outputCRT.material;
		if(outputMaterial) {
			if(outputAudio) {
				sampleStart = wclock;
				sampleStop = (int)outputAudio.GetProgramVariable("clock");
				wclock = sampleStop;
			}
			outputMaterial.SetInt("_SampleStart", sampleStart);
			outputMaterial.SetInt("_SampleStop", sampleStop);
			foreach(var mat in outputSyncMaterials) {
				mat.SetInt("_SampleStart", sampleStart);
				mat.SetInt("_SampleStop", sampleStop);
			}
		}
		if(inputMaterial) {
			if(inputAudio) {
				sampleStart = rclock;
				sampleStop = (int)inputAudio.GetProgramVariable("clock");
				rclock = sampleStop;

				inputMaterial.SetFloat("_Attenuation", (float)inputAudio.GetProgramVariable("attenuation"));

				System.Array.Copy(rbuffer, offsetX0, temp0, 0, maxArraySize);
				inputMaterial.SetFloatArray("_InputX0", temp0);
				System.Array.Copy(rbuffer, offsetX1, temp0, 0, maxArraySize);
				inputMaterial.SetFloatArray("_InputX1", temp0);
				System.Array.Copy(rbuffer, offsetX2, temp0, 0, maxArraySize);
				inputMaterial.SetFloatArray("_InputX2", temp0);
				System.Array.Copy(rbuffer, offsetX3, temp0, 0, maxArraySize);
				inputMaterial.SetFloatArray("_InputX3", temp0);
				System.Array.Copy(rbuffer, offsetX4, temp4, 0, remArraySize);
				inputMaterial.SetFloatArray("_InputX4", temp4);

				System.Array.Copy(rbuffer, offsetY0, temp0, 0, maxArraySize);
				inputMaterial.SetFloatArray("_InputY0", temp0);
				System.Array.Copy(rbuffer, offsetY1, temp0, 0, maxArraySize);
				inputMaterial.SetFloatArray("_InputY1", temp0);
				System.Array.Copy(rbuffer, offsetY2, temp0, 0, maxArraySize);
				inputMaterial.SetFloatArray("_InputY2", temp0);
				System.Array.Copy(rbuffer, offsetY3, temp0, 0, maxArraySize);
				inputMaterial.SetFloatArray("_InputY3", temp0);
				System.Array.Copy(rbuffer, offsetY4, temp4, 0, remArraySize);
				inputMaterial.SetFloatArray("_InputY4", temp4);
			} else {
				inputMaterial.SetFloat("_Attenuation", 1f);
			}
			inputMaterial.SetInt("_SampleStart", sampleStart);
			inputMaterial.SetInt("_SampleStop", sampleStop);
			foreach(var mat in inputSyncMaterials) {
				mat.SetInt("_SampleStart", sampleStart);
				mat.SetInt("_SampleStop", sampleStop);
			}
		}
	}
	private void OnPostRender() {
		if(outputAudio) {
			stopWatch.Restart();
			tempTex.ReadPixels(rect, 0, 0, false);
			System.Array.Copy(tempTex.GetPixels(), wbuffer, wbuffer.Length);
			stopWatch.Stop();
			if(debugText) {
				avgTime = Mathf.Lerp(avgTime, (float)stopWatch.Elapsed.TotalMilliseconds, 0.1f);
				debugText.text = string.Format("readback: {0:F2} ms, fps: {1:F0} Hz", avgTime, 1f/Time.smoothDeltaTime);
			}
		}
	}
}
}