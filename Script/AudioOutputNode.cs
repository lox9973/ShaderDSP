using UnityEngine;
using UdonSharp;
using VRC.Udon;
using VRC.SDKBase;
namespace ShaderAudio {
[DefaultExecutionOrder(1)]
[UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
public class AudioOutputNode: UdonSharpBehaviour {
	public Material material;
	public UdonBehaviour audioInterface;
	public CustomRenderTexture renderTexture;
	public Material[] syncedMaterials;

	[System.NonSerialized] public int clock;
	private Color[] wbuffer;
	private Texture2D tempTex;
	private Rect rect;

	public UnityEngine.UI.Text debugText;
	private System.Diagnostics.Stopwatch stopWatch;
	private float avgTime;
	private void OnEnable() {
		Debug.Log(string.Format("enable AudioOutputNode: {0}", audioInterface));
		renderTexture.material = material;
		if(audioInterface) {
			rect = new Rect(0, 0, renderTexture.width, renderTexture.height);
			wbuffer = new Color[renderTexture.width * renderTexture.height];
			tempTex = (Texture2D)renderTexture.initializationTexture;
			audioInterface.SetProgramVariable("clock", clock);
			audioInterface.SetProgramVariable("wbuffer", wbuffer);

			var camera = GetComponent<Camera>();
			camera.clearFlags = CameraClearFlags.Nothing;
			camera.cullingMask = 0;
			camera.allowMSAA = false;
			camera.targetTexture = renderTexture;
		}
		stopWatch = new System.Diagnostics.Stopwatch();
	}
	private void OnDisable() {
		Debug.Log(string.Format("disable AudioOutputNode: {0}", audioInterface));
		if(audioInterface) {
			audioInterface.SetProgramVariable("wbuffer", null);
		}
	}
	private void LateUpdate() {
		var lastClock = clock;
		if(audioInterface)
			clock = (int)audioInterface.GetProgramVariable("clock");
		material.SetInt("_SampleStart", lastClock);
		material.SetInt("_SampleStop", clock);
		foreach(var mat in syncedMaterials) {
			mat.SetInt("_SampleStart", lastClock);
			mat.SetInt("_SampleStop", clock);
		}
	}
	private void OnPostRender() {
		if(audioInterface) {
			stopWatch.Restart();
			tempTex.ReadPixels(rect, 0, 0, false);
			System.Array.Copy(tempTex.GetPixels(), wbuffer, wbuffer.Length);
			stopWatch.Stop();
			if(debugText) {
				avgTime = Mathf.Lerp(avgTime, (float)stopWatch.Elapsed.TotalMilliseconds, 0.1f);
				debugText.text = string.Format("readback: {0:F2} ms, fps: {1:F0} Hz", avgTime, 1f/Time.smoothDeltaTime);
			}
		}
	}
}
}