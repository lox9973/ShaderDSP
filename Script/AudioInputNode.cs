using UnityEngine;
using UdonSharp;
using VRC.Udon;
using VRC.SDKBase;
namespace ShaderAudio {
[DefaultExecutionOrder(2)]
[UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
public class AudioInputNode: UdonSharpBehaviour {
	public Material material;
	public UdonBehaviour audioInterface;
	public CustomRenderTexture renderTexture;
	public AudioOutputNode outputNode;
	public Material[] syncedMaterials;

	[System.NonSerialized] public int clock;
	private float[] rbuffer;

	const int sampleSize = 4096;
	const int maxArraySize = 1023, remArraySize = sampleSize % maxArraySize;
	const int offsetX0 = maxArraySize*0, offsetY0 = sampleSize+maxArraySize*0;
	const int offsetX1 = maxArraySize*1, offsetY1 = sampleSize+maxArraySize*1;
	const int offsetX2 = maxArraySize*2, offsetY2 = sampleSize+maxArraySize*2;
	const int offsetX3 = maxArraySize*3, offsetY3 = sampleSize+maxArraySize*3;
	const int offsetX4 = maxArraySize*4, offsetY4 = sampleSize+maxArraySize*4;
	private float[] temp0, temp4;
	private void Start() {
		temp0 = new float[maxArraySize];
		temp4 = new float[remArraySize];
	}

	private void OnEnable() {
		Debug.Log(string.Format("enable AudioInputNode: {0}", audioInterface));
		renderTexture.material = material;
		if(audioInterface) {
			clock = outputNode.clock;
			rbuffer = new float[sampleSize*2];
			audioInterface.SetProgramVariable("clockSource", outputNode.audioInterface);
			audioInterface.SetProgramVariable("clock", clock);
			audioInterface.SetProgramVariable("rbuffer", rbuffer);
			material.SetInt("_Interleave", (bool)audioInterface.GetProgramVariable("interleave") ? 1 : 0);
		}
	}
	private void OnDisable() {
		Debug.Log(string.Format("disable AudioInputNode: {0}", audioInterface));
		if(audioInterface) {
			audioInterface.SetProgramVariable("clockSource", null);
			audioInterface.SetProgramVariable("rbuffer", null);
		}
	}
	private void LateUpdate() {
		var lastClock = clock;
		if(audioInterface) {
			clock = (int)audioInterface.GetProgramVariable("clock");
			material.SetFloat("_Attenuation", (float)audioInterface.GetProgramVariable("attenuation"));

			System.Array.Copy(rbuffer, offsetX0, temp0, 0, maxArraySize);
			material.SetFloatArray("_InputX0", temp0);
			System.Array.Copy(rbuffer, offsetX1, temp0, 0, maxArraySize);
			material.SetFloatArray("_InputX1", temp0);
			System.Array.Copy(rbuffer, offsetX2, temp0, 0, maxArraySize);
			material.SetFloatArray("_InputX2", temp0);
			System.Array.Copy(rbuffer, offsetX3, temp0, 0, maxArraySize);
			material.SetFloatArray("_InputX3", temp0);
			System.Array.Copy(rbuffer, offsetX4, temp4, 0, remArraySize);
			material.SetFloatArray("_InputX4", temp4);

			System.Array.Copy(rbuffer, offsetY0, temp0, 0, maxArraySize);
			material.SetFloatArray("_InputY0", temp0);
			System.Array.Copy(rbuffer, offsetY1, temp0, 0, maxArraySize);
			material.SetFloatArray("_InputY1", temp0);
			System.Array.Copy(rbuffer, offsetY2, temp0, 0, maxArraySize);
			material.SetFloatArray("_InputY2", temp0);
			System.Array.Copy(rbuffer, offsetY3, temp0, 0, maxArraySize);
			material.SetFloatArray("_InputY3", temp0);
			System.Array.Copy(rbuffer, offsetY4, temp4, 0, remArraySize);
			material.SetFloatArray("_InputY4", temp4);
		} else {
			clock = outputNode.clock;
			material.SetFloat("_Attenuation", 1f);
		}
		material.SetInt("_SampleStart", lastClock);
		material.SetInt("_SampleStop", clock);
		foreach(var mat in syncedMaterials) {
			mat.SetInt("_SampleStart", lastClock);
			mat.SetInt("_SampleStop", clock);
		}
	}
}
}