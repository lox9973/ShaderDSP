using UnityEngine;
using UdonSharp;
namespace ShaderAudio {
[UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
public class AudioClipLoader: UdonSharpBehaviour {
	public AudioClip clip;
	public Texture2D texture;

	const int chunkSize = 1024;
	private int loadStep = -1;
	private int duration, channels;
	private float[] samples;
	private Color[] colors;
	private float sum;
	private int i;
	private void OnEnable() {
		loadStep = 0;
	}
	private void Update() {
		if(loadStep == 0) {
			channels = clip.channels;
			duration = Mathf.Min(clip.samples, texture.width * texture.height);
			samples = new float[duration * channels];
			colors = new Color[texture.width * texture.height];
			clip.LoadAudioData();
			clip.GetData(samples, 0);
			{
				sum = 0f; i = 0;
				loadStep ++;
			}
		} else if(loadStep == 1) {
			var offset1 = channels == 2 ? 1 : 0;
			var limit = Mathf.Min(duration, i+chunkSize);
			for(; i<limit; i++) {
				var j = i*channels;
				sum += samples[j]*samples[j]; j+=offset1;
				sum += samples[j]*samples[j];
			}
			if(limit == duration) {
				sum = Mathf.Sqrt(sum); i = 0;
				loadStep ++;
			}
		} else if(loadStep == 2) {
			var offset1 = channels == 2 ? 1 : 0;
			var limit = Mathf.Min(duration, i+chunkSize);
			for(; i<limit; i++) {
				var j = i*channels;
				colors[i].r = samples[j]/sum; j+=offset1;
				colors[i].g = samples[j]/sum;
			}
			if(limit == duration) {
				texture.SetPixels(colors, 0);
				texture.Apply(true);
				Debug.Log($"{clip} loaded: sum={sum}");
				loadStep ++;
			}
		} else
			enabled = false;
	}
}
}