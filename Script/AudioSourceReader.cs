using UnityEngine;
using UdonSharp;
namespace ShaderAudio {
[UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
public class AudioSourceReader : UdonSharpBehaviour {
	public AudioSource audioSource;
	public VRC.Udon.UdonBehaviour clockSource;
	public bool exclusive = true;

	[System.NonSerialized] public int clock;
	[System.NonSerialized] public float[] rbuffer; // stereo layout: L..L R..R
	[System.NonSerialized] public Color[] wbuffer; // unused
	[System.NonSerialized] public bool interleave = false;
	[System.NonSerialized] public float attenuation = 1f;

	private void OnAudioSourceRead(int ndata) {
		var rbuf = rbuffer;
		if(rbuf != null)  {
			var nbuf = rbuf.Length/2;
			var n = Mathf.Min(ndata, nbuf);
			var j = maxDataSize-n;
			var i = (clock-n) % nbuf;
			var m = Mathf.Min(n, nbuf-i);
			System.Array.Copy(data0, j,   rbuf, i,      m);
			System.Array.Copy(data0, j+m, rbuf, 0,      n-m);
			System.Array.Copy(data1, j,   rbuf, nbuf+i, m);
			System.Array.Copy(data1, j+m, rbuf, nbuf,   n-m);
		}
		UpdateVolume(ndata, rbuf != null && exclusive);
	}
	private bool lastMute = false;
	private float lastVolume, baseVolume;
	private int lifetime;
	private void UpdateVolume(int ndata, bool mute) {
		var volume = audioSource.volume;
		if(volume != lastVolume) {
			baseVolume = volume;
			lastMute = false;
		}
		if(lastMute)
			lifetime -= ndata;
		else
			lifetime = maxDataSize;
		attenuation = lifetime <= 0 ? 1000f : 1f;
		lastMute = mute;
		lastVolume = baseVolume / (mute ? 1000f : 1f);
		if(lastVolume != volume)
			audioSource.volume = lastVolume;
	}

	private const int maxDataSize = 4096;
	private float[] data0, data1;
	private void Start() {
		data0 = new float[maxDataSize];
		data1 = new float[maxDataSize];
	}
	private void Update() {
		if(!clockSource) {
			UpdateVolume(0, false);
			return;
		}
		const int lastIndex = maxDataSize-1;
		var data0Last = data0[lastIndex];
		audioSource.GetOutputData(data0, 0);
		audioSource.GetOutputData(data1, 1);
		var delta = (int)clockSource.GetProgramVariable("clock") - clock;
		var testIndex = lastIndex - delta;
		if(0 <= testIndex && testIndex < maxDataSize && data0[testIndex] != data0Last) {
			if((testIndex -= 1024) >= 0 && data0[testIndex] == data0Last)
				delta = lastIndex - testIndex;
			else if((testIndex += 2048) < maxDataSize && data0[testIndex] == data0Last)
				delta = lastIndex - testIndex;
			else if((testIndex -= 3072) >= 0 && data0[testIndex] == data0Last)
				delta = lastIndex - testIndex;
			else
				Debug.Log("unable to resolve drift");
		}
		clock += delta;
		var ndata = Mathf.Min(delta, maxDataSize);
		if(ndata > 0)
			OnAudioSourceRead(ndata);
	}
}
}